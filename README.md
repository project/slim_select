INTRODUCTION
------------

Integrates [Slim Select](https://github.com/brianvoe/slim-select) with the select form element, converting it into a
Slim Select HTML element. Allows users to add classes to the select options and perform search on them.

* Homepage: https://www.drupal.org/project/slim_select
* External library homepage: https://slimselectjs.com
* Bugs and improvements: https://www.drupal.org/project/issues/slim_select

INSTALLATION
------------

### Preferred method:
Slim Select is providing a package.json that can include the slim-select library. Make sure you have NPM of nodejs
installed in your system and from the module folder, run `npm install`.
Alternatively, in your composer add the following script in your post-install-cmd scripts in order to install the
dependencies with each `composer install`.
```json
{
  "scripts": {
    "post-install-cmd": [
      "npm install -C web/modules/contrib/slim_select"
    ]
  }
}
```
In the script above, it is assumed that the `slim_select` module is installed in the `web/modules/contrib` directory as
a relative path from the project root or where your `composer.json` is located. Adapt the path according to your installation.
### Alternative methods:
Manually download and install the library in the `libraries` folder in the webroot, profile directory or site directory
within drupal. The package should be placed as such, so that the path looks like
`<libraries path>/slim-select/dist/slimselect.min.js` where `<libraries path>` is one of the directories mentioned
above.

USAGE
-----

In order to override the normal select element and display a Slim Select version, simply add a `#slim_select` property
to the existing form element like below.

```php
$form['my_slim_select'] = [
  '#type' => 'select'
  '#title' => 'A new select',
  '#options' => [
    'option1' => 'Red',
    'option2' => 'Green',
    'option3' => 'Blue',
  ],
  '#slim_select' => [
    'showSearch' => TRUE,
  ],
];
```

The `#slim_select` entry holds the configurable options of the Slim Select library. There are two configurable options
currently supported as well.
- `showSearch`. Controls whether there will be a search bar for looking up options in the select.
- `selectByGroup`. When the select element accepts multiple options, if there are options grouped by an optgroup, then
the optgroup becomes clickable, and clicking it selects automatically all children options.

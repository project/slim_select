/**
 * @file
 * Use Slim Select JS.
 */

(function (Drupal, once, drupalSettings) {
  Drupal.behaviors.slimSelect = {
    attach(context) {
      const version = drupalSettings.slimSelectVersion;
      once('slimSelect', '[data-drupal-slim-select]', context).forEach(function ($selectElement) {
        let attr = $selectElement.getAttribute('data-drupal-slim-select');
        attr = JSON.parse(attr);
        let options = [];
        let select = '#' + $selectElement.getAttribute('id');
        let showSearch = (typeof attr['showSearch'] === 'undefined') ? 'TRUE' : attr['showSearch']
        let searchPlaceholder = Drupal.t('Search');
        let searchText = Drupal.t('No Results');
        let searchingText = Drupal.t('Searching...');
        let placeholder = Drupal.t('Select Value');
        if (version.startsWith('v1.')) {
          options = {
            select: select,
            showSearch: showSearch,
            selectByGroup: (typeof attr['selectByGroup'] === 'undefined') ? 'TRUE' : attr['selectByGroup'],
            searchPlaceholder: searchPlaceholder,
            searchText: searchText,
            searchingText: searchingText,
            placeholder: placeholder
          };
        }
        else if (version.startsWith('v2.')) {
          options = {
            select: select,
            settings: {
              showSearch: showSearch,
              searchPlaceholder: searchPlaceholder,
              searchText: searchText,
              searchingText: searchingText,
              placeholder: placeholder
            }
          };
        }
        new SlimSelect(options);
      });
    }
  };
})(Drupal, once, drupalSettings);

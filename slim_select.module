<?php

/**
 * @file
 * Primary module hooks for Slim Select module.
 */

declare(strict_types=1);

use Drupal\slim_select\SlimSelectFormRender;

/**
 * Implements hook_element_info_alter().
 */
function slim_select_element_info_alter(array &$info): void {
  $info['select']['#pre_render'][] = [
    SlimSelectFormRender::class,
    'preRenderSelect',
  ];
}

/**
 * Implements hook_library_info_alter().
 */
function slim_select_library_info_alter(array &$libraries, string $module): void {
  if ('slim_select' !== $module || !isset($libraries['slim.select'])) {
    return;
  }

  _slim_select_get_cdn_library($libraries);

  if ($path = _slim_select_get_path()) {
    _slim_select_get_local_library($libraries, $path);
  }
}

/**
 * Get the location of the slim-select library.
 *
 * @return string
 *   The location of the library, or FALSE if the library isn't installed.
 */
function _slim_select_get_path(): ?string {
  // The default way of installing the Slim Select is through npm in the module
  // directory. Search primarily for the node_modules directory within the
  // module.
  $module_handler = \Drupal::getContainer()->get('module_handler');
  $module_path = $module_handler->getModule('slim_select')->getPath();
  if (file_exists($module_path . '/node_modules/slim-select/dist/slimselect.min.js')) {
    return $module_path . '/node_modules/slim-select/dist';
  }

  // If the library was not installed by the recommended way, look for other
  // places where the library can be found.
  if (function_exists('libraries_get_path') && $libraries_path = libraries_get_path('slim-select')) {
    return $libraries_path;
  }

  // The following logic is taken from libraries_get_libraries()
  $searchdir = [];

  // Similar to 'modules' and 'themes' directories inside an installation
  // profile, installation profiles may want to place libraries into a
  // 'libraries' directory.
  $searchdir[] = 'profiles/' . \Drupal::installProfile() . '/libraries';

  // Always search libraries.
  $searchdir[] = 'libraries';

  // Also search sites/<domain>/*.
  $searchdir[] = \Drupal::getContainer()->getParameter('site.path') . '/libraries';

  foreach ($searchdir as $dir) {
    if (file_exists($dir . '/slim-select/dist/slimselect.min.js')) {
      return $dir . '/slim-select/dist';
    }
  }

  return NULL;
}

/**
 * Gets the CDN library properties.
 *
 * @param array $libraries
 *   The libraries.
 */
function _slim_select_get_cdn_library(array &$libraries) {
  $version = \Drupal::config('slim_select.settings')->get('version');
  $trimmed_version = ltrim($version, 'v');
  $properties = [
    'type' => 'external',
    'minified' => TRUE,
    'attributes' => [
      'defer' => TRUE,
    ],
  ];
  $url = "https://cdnjs.cloudflare.com/ajax/libs/slim-select/{$trimmed_version}/";

  $libraries['slim.select']['js'] = [
    $url . 'slimselect.min.js' => $properties,
  ];

  $libraries['slim.select']['css'] = [
    'component' => [
      $url . 'slimselect.min.css' => $properties,
    ],
  ];

  $libraries['slim.select']['version'] = $version;
}

/**
 * Gets the local library properties.
 *
 * @param array $libraries
 *   The libraries.
 * @param string $path
 *   The path to the library.
 */
function _slim_select_get_local_library(array &$libraries, string $path) {
  $minified = file_exists($path . '/slimselect.min.js');
  $js_path = '/' . $path;
  $js = $minified
    ? $js_path . '/slimselect.min.js'
    : $js_path . '/slimselect.js';
  $libraries['slim.select']['js'] = [
    $js => [
      'minified' => $minified,
      'attributes' => [
        'defer' => TRUE,
      ],
    ],
  ];
  $minified_css = file_exists($path . '/slimselect.min.css');
  $css_path = '/' . $path;
  $css = $minified_css
    ? $css_path . '/slimselect.min.css'
    : $css_path . '/slimselect.css';
  $libraries['slim.select']['css']['component'] = [
    $css => [
      'minified' => $minified,
      'attributes' => [
        'defer' => TRUE,
      ],
    ],
  ];
}

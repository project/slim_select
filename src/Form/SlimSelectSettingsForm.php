<?php

namespace Drupal\slim_select\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the module.
 */
class SlimSelectSettingsForm extends ConfigFormBase {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, CacheTagsInvalidatorInterface $cache_tags_invalidator, ClientInterface $http_client, CacheBackendInterface $cache_backend, LoggerInterface $logger) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->httpClient = $http_client;
    $this->cacheBackend = $cache_backend;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('cache_tags.invalidator'),
      $container->get('http_client'),
      $container->get('cache.default'),
      $container->get('logger.factory')->get('slim_select')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slim_select_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['slim_select.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['version'] = [
      '#title' => $this->t('Version'),
      '#type' => 'select',
      '#config_target' => 'slim_select.settings:version',
      '#required' => TRUE,
      '#description' => $this->t('Select the version of the Slim Select library to be used.'),
      '#options' => $this->getVersionOptions(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Invalidate the cache for libraries.
    $this->cacheTagsInvalidator->invalidateTags(['library_info']);
  }

  /**
   * Get the version field options.
   *
   * @return array
   *   The version options.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function getVersionOptions(): array {
    $cid = 'slim_select:settings_tags';
    $options = [];

    if ($cache = $this->cacheBackend->get($cid)) {
      $options = $cache->data;
    }
    else {
      try {
        $request = $this->httpClient->get('https://api.github.com/repos/brianvoe/slim-select/tags', [
          'timeout' => 5,
        ]);
        if ($request->getStatusCode() === 200) {
          $tags = json_decode($request->getBody(), TRUE);

          foreach ($tags as $tag) {
            $options[$tag['name']] = Xss::filter($tag['name']);
          }

          $this->cacheBackend->set($cid, $options);
        }
      }
      catch (\Exception $e) {
        $this->messenger()->addError($this->t('An error occurred while fetching the tags from GitHub. Please, try again later.'));
        $this->logger->error($e->getMessage());
      }
    }

    return $options;
  }

}

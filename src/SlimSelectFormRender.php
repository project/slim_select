<?php

declare(strict_types=1);

namespace Drupal\slim_select;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Trusted callback for processing the form element.
 */
class SlimSelectFormRender implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'preRenderSelect',
    ];
  }

  /**
   * Render API callback: Apply Slim Select properties to a select element.
   *
   * @param array $element
   *   The element.
   *
   * @return array
   *   The element.
   */
  public static function preRenderSelect(array $element): array {
    if (isset($element['#slim_select'])) {
      if (!is_array($element['#slim_select'])) {
        throw new \RuntimeException('Element entry "#slim_select" expects to be an array.');
      }

      $version = \Drupal::config('slim_select.settings')->get('version');
      $element['#attached']['drupalSettings']['slimSelectVersion'] = $version;
      $element['#attached']['library'][] = 'slim_select/select';
      $element['#attributes']['data-drupal-slim-select'] = json_encode($element['#slim_select']);
    }
    return $element;
  }

}

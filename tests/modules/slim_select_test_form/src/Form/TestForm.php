<?php

declare(strict_types=1);

namespace Drupal\slim_select_test_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Builds a form to test the Slim Select element override.
 */
class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'slim_select_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $options = [
      'foo' => '<span>foo</span>',
      'bar' => 'baz',
      'lay' => 'low',
      'group' => [
        'hello' => 'world',
        'some' => 'option',
      ],
    ];

    $form['default_settings'] = [
      '#type' => 'select',
      '#title' => 'Default settings',
      '#options' => $options,
      '#slim_select' => [],
    ];

    $form['no_search'] = [
      '#type' => 'select',
      '#title' => 'No search',
      '#options' => $options,
      '#slim_select' => [
        'showSearch' => FALSE,
      ],
    ];

    $form['no_settings_enabled'] = [
      '#type' => 'select',
      '#title' => 'No settings enabled',
      '#options' => $options,
      '#multiple' => TRUE,
      '#slim_select' => [
        'showSearch' => FALSE,
        'selectByGroup' => FALSE,
      ],
    ];

    $form['group_select_multiple'] = [
      '#type' => 'select',
      '#title' => 'Group select multiple',
      '#options' => $options,
      '#multiple' => TRUE,
      '#slim_select' => [
        'selectByGroup' => TRUE,
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus('The form has been submitted.');
    $form_state->setResponse(new JsonResponse($form_state->getValues()));
  }

}

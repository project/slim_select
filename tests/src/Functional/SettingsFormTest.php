<?php

declare(strict_types=1);

namespace Drupal\Tests\slim_select\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the Slim Select configuration form.
 *
 * @group slim_select
 */
class SettingsFormTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'slim_select_test_form',
    'dblog',
    'filter',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the Slim Select settings form.
   */
  public function testSettingsForm(): void {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Anonymous doesn't have access to the page.
    $this->drupalGet('admin/config/content/slim-select');
    $assert_session->pageTextContains('You are not authorized to access this page.');

    // User without permission doesn't have access to the page.
    $this->drupalLogin($this->createUser());
    $this->drupalGet('admin/config/content/slim-select');
    $assert_session->pageTextContains('You are not authorized to access this page.');

    $this->drupalLogin($this->createUser(['administer slim select configuration']));
    $this->drupalGet('admin/config/content/slim-select');

    $assert_session->selectExists('version');
    $assert_session->fieldValueEquals('version', 'v2.10.0');

    $this->drupalGet('/slim-select');
    $this->assertStringContainsString('https://cdnjs.cloudflare.com/ajax/libs/slim-select/2.10.0/slimselect.min.css', $page->getHtml());
    $assert_session->elementNotExists('css', '.ss-single-selected');
    $assert_session->elementNotExists('css', '.ss-multi-selected');

    $this->drupalGet('admin/config/content/slim-select');
    $this->getSession()->getPage()->selectFieldOption('version', 'v1.27.1');
    $page->pressButton('Save configuration');
    $assert_session->pageTextContains('The configuration options have been saved.');
    $assert_session->fieldValueEquals('version', 'v1.27.1');
    $this->drupalGet('/slim-select');
    $this->assertStringContainsString('https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.1/slimselect.min.css', $page->getHtml());
    $assert_session->elementExists('css', '.ss-single-selected');
    $assert_session->elementExists('css', '.ss-multi-selected');
  }

}

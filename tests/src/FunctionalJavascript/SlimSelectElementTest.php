<?php

declare(strict_types=1);

namespace Drupal\Tests\slim_select\FunctionalJavascript;

use Drupal\Component\Serialization\Json;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests various form element validation mechanisms.
 *
 * @group slim_select
 */
class SlimSelectElementTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'slim_select_test_form',
    'dblog',
    'filter',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'starterkit_theme';

  /**
   * Tests that Slim Select elements are being replaced properly.
   */
  public function testSelectReplacement(): void {
    $this->drupalGet('slim-select');
    $fields = [
      'Default settings',
      'No search',
      'No settings enabled',
      'Group select multiple',
    ];

    foreach ($fields as $field) {
      $field = $this->assertSession()->fieldExists($field);
      $this->assertIsObject($field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]'));
    }
  }

  /**
   * Tests the form behavior of the element as well as the submitted values.
   */
  public function testSubmitValues(): void {
    $this->drupalGet('/slim-select');
    // By default, the Slim Select contains a search bar.
    $default_settings_field = $this->assertSession()->fieldExists('Default settings');
    $default_settings_ss_main = $default_settings_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $default_settings_ss_id = $default_settings_ss_main->getAttribute('data-id');
    $default_settings_ss_content_xpath = sprintf('//div[contains(@class, "ss-content") and @data-id="%s"]', $default_settings_ss_id);
    $default_settings_ss_content = $this->getSession()->getPage()->find('xpath', $default_settings_ss_content_xpath);
    $this->assertIsObject($default_settings_ss_content->find('xpath', '//div[@class="ss-search"]'));
    // The classic option select does not work with Slim Select as it looks for
    // option elements and Slim Select does not use them. Open the dropdown to
    // manually select the options.
    $default_settings_ss_main->click();
    // Slim Select has minor delay in opening and closing the dialog so add a
    // small delay to ensure that tests do not break due to fast automated
    // actions.
    $this->getSession()->wait(1000);
    $default_settings_ss_content->find('xpath', '//div[contains(@class, "ss-option") and text()="world"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($default_settings_ss_main->find('xpath', '//div[contains(@class, "ss-single") and text()="world"]'));

    // Test that a search can be hidden.
    $no_search_field = $this->assertSession()->fieldExists('No search');
    $no_search_ss_main = $no_search_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $no_search_ss_id = $no_search_ss_main->getAttribute('data-id');
    $no_search_ss_content_xpath = sprintf('//div[contains(@class, "ss-content") and @data-id="%s"]', $no_search_ss_id);
    $no_search_ss_content = $this->getSession()->getPage()->find('xpath', $no_search_ss_content_xpath);
    $this->assertIsObject($no_search_ss_content->find('xpath', '//div[@class="ss-search ss-hide"]'));
    $no_search_ss_main->click();
    $this->getSession()->wait(1000);
    $no_search_ss_content->find('xpath', '//div[contains(@class, "ss-option") and text()="low"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($no_search_ss_main->find('xpath', '//div[contains(@class, "ss-single") and text()="low"]'));

    // Test that a search can be hidden in multiple type of select field.
    $no_search_multiple_field = $this->assertSession()->fieldExists('No settings enabled');
    $no_search_multiple_ss_main = $no_search_multiple_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $no_search_multiple_ss_id = $no_search_multiple_ss_main->getAttribute('data-id');
    $no_search_multiple_ss_content_xpath = sprintf('//div[contains(@class, "ss-content") and @data-id="%s"]', $no_search_multiple_ss_id);
    $no_search_multiple_ss_content = $this->getSession()->getPage()->find('xpath', $no_search_multiple_ss_content_xpath);
    $this->assertIsObject($no_search_multiple_ss_content->find('xpath', '//div[@class="ss-search ss-hide"]'));
    $this->assertEmpty($no_search_multiple_ss_content->find('xpath', '//div[@class="ss-optgroup-label"]//div[@class="ss-optgroup-actions"]//div[@class="ss-selectall"]'));
    $no_search_multiple_ss_main->click();
    $this->getSession()->wait(1000);
    $no_search_multiple_ss_content->find('xpath', '//div[contains(@class, "ss-option") and text()="<span>foo</span>"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($no_search_multiple_ss_main->find('xpath', '//div[contains(@class, "ss-values")]//div[@class="ss-value-text" and text()="<span>foo</span>"]'));

    // Test that options inside the optgroup can be selected.
    $group_select_field = $this->assertSession()->fieldExists('Group select multiple');
    $group_select_ss_main = $group_select_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $group_select_ss_id = $group_select_ss_main->getAttribute('data-id');
    $group_select_ss_content_xpath = sprintf('//div[contains(@class, "ss-content") and @data-id="%s"]', $group_select_ss_id);
    $group_select_ss_content = $this->getSession()->getPage()->find('xpath', $group_select_ss_content_xpath);
    $optgroup_option = $group_select_ss_content->find('xpath', '//div[@class="ss-optgroup-label"]//div[@class="ss-optgroup-label-text" and text()="group"]');
    $this->assertIsObject($optgroup_option);
    $group_select_ss_main->click();
    $this->getSession()->wait(1000);
    $optgroup_option->getParent()->getParent()->find('xpath', '//div[contains(@class, "ss-option") and text()="world"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($group_select_ss_main->find('xpath', '//div[contains(@class, "ss-values")]//div[@class="ss-value-text" and text()="world"]'));
    $group_select_ss_main->click();
    $this->getSession()->wait(1000);
    $optgroup_option->getParent()->getParent()->find('xpath', '//div[contains(@class, "ss-option") and text()="option"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($group_select_ss_main->find('xpath', '//div[contains(@class, "ss-values")]//div[@class="ss-value-text" and text()="option"]'));

    $this->assertSubmittedValues();
  }

  /**
   * Asserts the submitted values.
   */
  protected function assertSubmittedValues() {
    $this->submitForm([], 'Submit');
    $values = Json::decode(strip_tags($this->getSession()->getPage()->getContent()));
    unset($values['submit']);
    unset($values['form_build_id']);
    unset($values['form_id']);
    unset($values['op']);

    $expected = [
      'default_settings' => 'hello',
      'no_search' => 'lay',
      'no_settings_enabled' => ['foo' => 'foo'],
      'group_select_multiple' => ['hello' => 'hello', 'some' => 'some'],
    ];
    $this->assertSame($expected, $values);
  }

}

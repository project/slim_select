<?php

declare(strict_types=1);

namespace Drupal\Tests\slim_select\FunctionalJavascript;

/**
 * Tests various form element validation mechanisms.
 *
 * @group slim_select
 */
class SlimSelectV1ElementTest extends SlimSelectElementTest {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('slim_select.settings')->set('version', 'v1.27.1')->save();
  }

  /**
   * Tests the form behavior of the element as well as the submitted values.
   */
  public function testSubmitValues(): void {
    $this->drupalGet('/slim-select');

    // By default, the Slim Select contains a search bar.
    $default_settings_field = $this->assertSession()->fieldExists('Default settings');
    $default_settings_slim_select = $default_settings_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $this->assertIsObject($default_settings_slim_select->find('xpath', '//div[@class="ss-content"]/div[@class="ss-search"]'));
    // The classic option select does not work with Slim Select as it looks for
    // option elements and Slim Select does not use them. Open the dropdown to
    // manually select the options.
    $default_settings_slim_select->click();
    // Slim Select has minor delay in opening and closing the dialog so add a
    // small delay to ensure that tests do not break due to fast automated
    // actions.
    $this->getSession()->wait(1000);
    $default_settings_slim_select->find('xpath', '//div[contains(@class, "ss-option") and text()="world"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($default_settings_slim_select->find('xpath', '//div[contains(@class, "ss-single-selected")]//span[@class="placeholder" and text()="world"]'));

    // Test that a search can be hidden.
    $no_search_field = $this->assertSession()->fieldExists('No search');
    $no_search_slim_select = $no_search_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $this->assertIsObject($no_search_slim_select->find('xpath', '//div[@class="ss-content"]/div[@class="ss-search ss-hide"]'));
    $no_search_slim_select->click();
    $this->getSession()->wait(1000);
    $no_search_slim_select->find('xpath', '//div[contains(@class, "ss-option") and text()="low"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($no_search_slim_select->find('xpath', '//div[contains(@class, "ss-single-selected")]//span[@class="placeholder" and text()="low"]'));

    // Test that both the search bar and the select by group options are
    // disabled.
    $no_settings_field = $this->assertSession()->fieldExists('No settings enabled');
    $no_settings_slim_select = $no_settings_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $this->assertIsObject($no_settings_slim_select->find('xpath', '//div[@class="ss-content"]/div[@class="ss-search ss-hide"]'));
    $this->assertEmpty($no_settings_slim_select->find('xpath', '//div[@class="ss-optgroup-label ss-optgroup-label-selectable" and text()="group"]'));
    $no_settings_slim_select->click();
    $this->getSession()->wait(1000);
    $no_settings_slim_select->find('xpath', '//div[contains(@class, "ss-option") and text()="<span>foo</span>"]')->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($no_settings_slim_select->find('xpath', '//div[contains(@class, "ss-multi-selected")]//div[contains(@class, "ss-values")]//span[@class="ss-value-text" and text()="<span>foo</span>"]'));

    // Test that options can be selected by the optgroup label.
    $group_select_field = $this->assertSession()->fieldExists('Group select multiple');
    $group_select_slim_select = $group_select_field->getParent()->find('xpath', '//div[contains(@class, "ss-main")]');
    $optgroup_option = $group_select_slim_select->find('xpath', '//div[@class="ss-optgroup-label ss-optgroup-label-selectable" and text()="group"]');
    $this->assertIsObject($optgroup_option);
    $group_select_slim_select->click();
    $this->getSession()->wait(1000);
    $optgroup_option->click();
    $this->getSession()->wait(1000);
    $this->assertIsObject($group_select_slim_select->find('xpath', '//div[contains(@class, "ss-multi-selected")]//div[contains(@class, "ss-values")]//span[@class="ss-value-text" and text()="world"]'));
    $this->assertIsObject($group_select_slim_select->find('xpath', '//div[contains(@class, "ss-multi-selected")]//div[contains(@class, "ss-values")]//span[@class="ss-value-text" and text()="option"]'));

    $this->assertSubmittedValues();
  }

}
